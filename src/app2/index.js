import React from "react";
import ReactDOM from "react-dom";
import App from "./container/Home";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
